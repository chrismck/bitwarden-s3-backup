# Bitwarden S3 Backup

This container is heavily based on the work in [kubernetes-cloud-mysql-backup](https://github.com/benjamin-maynard/kubernetes-cloud-mysql-backup). All credit goes to the owner of and contributors to this repository.

This container is intended to run as a `CronJob` in Kubernetes, to perform a scheduled export of a Bitwarden account, and upload the export to a S3 bucket.

## Getting started

| Environment Variable   | Description                                                                                                                                                                                    |
|------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| BW_CLIENTID            | Bitwarden Client ID for API key authentication                                                                                                                                                 |
| BW_CLIENTSECRET        | Bitwarden API client secret                                                                                                                                                                    |
| BW_PASSWORD            | Bitwarden Vault master password                                                                                                                                                                |
| AWS_ACCESS_KEY_ID      | S3 Storage Access Key ID                                                                                                                                                                       |
| AWS_SECRET_ACCESS_KEY  | S3 Storage Secret Access Key                                                                                                                                                                   |
| AWS_DEFAULT_REGION     | S3 default region                                                                                                                                                                              |
| AWS_S3_ENDPOINT        | (Optional) S3 Endpoint. Can be used to backup to non-AWS S3 compatible storage                                                                                                                 |
| AWS_BUCKET_NAME        | S3 Bucket name to back up to                                                                                                                                                                   |
| AWS_BUCKET_BACKUP_PATH | Path within the S3 bucket to store backups in                                                                                                                                                  |
| BACKUP_TIMESTAMP       | (Optional) Date string to append to the backup filename ([date](https://man7.org/linux/man-pages/man1/date.1.html) format). Leave unset if using S3 Versioning and date stamp is not required. |