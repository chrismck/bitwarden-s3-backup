FROM python:3.9-alpine

RUN apk -v --update add \
    libc6-compat \
    gcompat \
    nodejs \
    npm \
    make \
    build-base && \
    pip3 install --upgrade awscli && \
    rm /var/cache/apk/*

RUN npm install -g @bitwarden/cli

COPY resources/perform-backup.sh /
RUN chmod +x /perform-backup.sh
CMD ["sh", "/perform-backup.sh"]