#!/bin/sh
set -e 

bw login --apikey
export BW_SESSION=$(bw unlock --passwordenv BW_PASSWORD --raw)

BACKUP_FILE=Backup$(date +$BACKUP_TIMESTAMP).csv

bw export --format csv --output /tmp/$BACKUP_FILE

if [ ! -z "$AWS_S3_ENDPOINT" ]; then
    ENDPOINT="--endpoint-url=$AWS_S3_ENDPOINT"
fi

if awsoutput=$(aws $ENDPOINT s3 cp /tmp/$BACKUP_FILE s3://$AWS_BUCKET_NAME$AWS_BUCKET_BACKUP_PATH/$DUMP 2>&1); then
    echo -e "Bitwarden export successfully uploaded at $(date +'%d-%m-%Y %H:%M:%S')."
else
    echo -e "Bitwarden export failed to upload at $(date +'%d-%m-%Y %H:%M:%S'). Error: $awsoutput"
fi

rm /tmp/"$BACKUP_FILE"
